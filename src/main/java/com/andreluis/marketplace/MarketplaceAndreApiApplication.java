package com.andreluis.marketplace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketplaceAndreApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarketplaceAndreApiApplication.class, args);
	}

}

